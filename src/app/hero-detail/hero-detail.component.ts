import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero } from '../hero';
import { HeroService } from '../services/hero.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero | undefined;

  constructor(private _route: ActivatedRoute,
    private _heroService: HeroService,
    private _location: Location) { }

  ngOnInit(): void {
    this.getHero();
  }

  getHero(): void {
    const id = + (this._route.snapshot.paramMap.get('id') || '0');
    this._heroService.getHero(id).subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this._location.back();
  }

  save(): void {
    if (this.hero){
      this._heroService.updateHero(this.hero)
      .subscribe(() => this.goBack());
    }
    
  }
}
